# VideoGameAPI

This is a test project for a salad.io interview.

It was made in Visual Studio 2019, using .NET Core 3.1, and implements the specs for [Salad's Backend Software Engineer Project](https://www.notion.so/Salad-s-Technical-Interview-bf17810d07994f559121c71260a2085a).

To make it work, you'll need to include a file called `secrets.json` in both the `VideoGameAPI` and `VideoGameAPI.Test` folders, with the following contents (replacing `your_api_key` with an actual RAWG API key):

	{
		"AppSettings": {
			"RawgApiKey": "your_api_key"
		}
	}

---

Contact Jacob Scharff with any questions.

