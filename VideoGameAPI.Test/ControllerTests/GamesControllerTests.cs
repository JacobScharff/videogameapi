using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controllers = VideoGameAPI.Controllers;
using Data = VideoGameAPI.Models.Data;
using Inputs = VideoGameAPI.Models.Inputs;
using Models = VideoGameAPI.Models;

namespace VideoGameAPI.Test.ControllerTests
{
	[TestClass]
	public class GamesControllerTests
	{
		#region [ Init ]

		[TestInitialize]
		public void Initialize()
		{
			//Create logger
			iLogger = new LoggerFactory().CreateLogger<Controllers.GamesController>();

			//Get secrets.json data
			var builder = new ConfigurationBuilder()
				.AddJsonFile($"secrets.json", optional: false, reloadOnChange: true)
				.Build();

			iAppSettings = new Models.AppSettings();
			builder.GetSection("AppSettings").Bind(iAppSettings);
		}

		#endregion

		#region [ Tests ]

		[TestMethod]
		public void Search_Should_Return_400_NoSearchString()
		{
			//Arrange
			var controller = new Controllers.GamesController(iLogger, iAppSettings);

			//Act
			var task = controller.Search("", null); //Search string cannot be empty.
			task.Wait();
			var result = task.Result;
			var objResult = result as BadRequestResult;

			//Assert
			Assert.IsNotNull(objResult);
			Assert.AreEqual(400, objResult.StatusCode);
		}

		[TestMethod]
		public void Search_Should_Return_400_BadSort()
		{
			//Arrange
			var controller = new Controllers.GamesController(iLogger, iAppSettings);

			//Act
			var task = controller.Search("Halo", "something_fake"); //Invalid sort option.
			task.Wait();
			var result = task.Result;
			var objResult = result as BadRequestResult;

			//Assert
			Assert.IsNotNull(objResult);
			Assert.AreEqual(400, objResult.StatusCode);
		}

		[TestMethod]
		public void Search_Should_Return_200()
		{
			//Arrange
			var controller = new Controllers.GamesController(iLogger, iAppSettings);

			//Act
			var task = controller.Search("Halo", null); //Good search request.
			task.Wait();
			var result = task.Result;
			var objResult = result as OkObjectResult;

			//Assert
			Assert.IsNotNull(objResult);
			Assert.AreEqual(200, objResult.StatusCode);
		}

		#endregion

		#region [ Mock Data ]

		private ILogger<Controllers.GamesController> iLogger;
		private Models.IAppSettings iAppSettings;

		#endregion
	}
}
