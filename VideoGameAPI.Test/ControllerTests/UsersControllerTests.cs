using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controllers = VideoGameAPI.Controllers;
using Data = VideoGameAPI.Models.Data;
using Inputs = VideoGameAPI.Models.Inputs;
using Models = VideoGameAPI.Models;
using Outputs = VideoGameAPI.Models.Outputs;

namespace VideoGameAPI.Test.ControllerTests
{
	[TestClass]
	public class UsersControllerTests
	{
		#region [ Init ]

		[TestInitialize]
		public void Initialize()
		{
			//Reset User data
			ClearData();

			//Create logger
			iLogger = new LoggerFactory().CreateLogger<Controllers.UsersController>();

			//Get secrets.json data
			var builder = new ConfigurationBuilder()
				.AddJsonFile($"secrets.json", optional: false, reloadOnChange: true)
				.Build();

			iAppSettings = new Models.AppSettings();
			builder.GetSection("AppSettings").Bind(iAppSettings);
		}

		[TestCleanup]
		public void Cleanup()
		{
			ClearData();
		}

		#endregion

		#region [ Tests ]

		[TestMethod]
		public void AddUser_Should_Return_201()
		{
			//Arrange
			ClearData();
			var controller = new Controllers.UsersController(iLogger, iUsers, iAppSettings);

			//Act
			var result = controller.AddUser();
			var objResult = result as ObjectResult;
			var value = objResult.Value as Data.User;

			//Assert
			Assert.IsNotNull(objResult);
			Assert.AreEqual(201, objResult.StatusCode);
			Assert.IsNotNull(value);
			Assert.AreEqual(1, value.userId);
		}

		[TestMethod]
		public void GetUser_Should_Return_404()
		{
			//Arrange
			MockData();
			var controller = new Controllers.UsersController(iLogger, iUsers, iAppSettings);

			//Act
			var result = controller.GetUser(5); //User shouldn't exist.
			var objResult = result as NotFoundResult;

			//Assert
			Assert.IsNotNull(objResult);
			Assert.AreEqual(404, objResult.StatusCode);
		}

		[TestMethod]
		public void GetUser_Should_Return_200()
		{
			//Arrange
			MockData();
			var controller = new Controllers.UsersController(iLogger, iUsers, iAppSettings);

			//Act
			var result = controller.GetUser(2); //User should exist.
			var objResult = result as OkObjectResult;
			var value = objResult.Value as Data.User;

			//Assert
			Assert.IsNotNull(objResult);
			Assert.AreEqual(200, objResult.StatusCode);
			Assert.IsNotNull(value);
			Assert.AreEqual(2, value.userId);
		}

		[TestMethod]
		public void AddGame_Should_Return_404()
		{
			//Arrange
			MockData();
			var controller = new Controllers.UsersController(iLogger, iUsers, iAppSettings);

			//Act
			var task = controller.AddGame(5, new Inputs.AddGameInput { gameId = 35 }); //User shouldn't exist.
			task.Wait();
			var result = task.Result;
			var objResult = result as NotFoundResult;

			//Assert
			Assert.IsNotNull(objResult);
			Assert.AreEqual(404, objResult.StatusCode);
		}

		[TestMethod]
		public void AddGame_Should_Return_409()
		{
			//Arrange
			MockData();
			var controller = new Controllers.UsersController(iLogger, iUsers, iAppSettings);

			//Act
			var task = controller.AddGame(1, new Inputs.AddGameInput { gameId = 31 }); //Game already exists in user's list.
			task.Wait();
			var result = task.Result;
			var objResult = result as ConflictResult;

			//Assert
			Assert.IsNotNull(objResult);
			Assert.AreEqual(409, objResult.StatusCode);
		}

		[TestMethod]
		public void AddGame_Should_Return_400()
		{
			//Arrange
			MockData();
			var controller = new Controllers.UsersController(iLogger, iUsers, iAppSettings);

			//Act
			var task = controller.AddGame(1, new Inputs.AddGameInput { gameId = -1 }); //Game doesn't exist.
			task.Wait();
			var result = task.Result;
			var objResult = result as BadRequestResult;

			//Assert
			Assert.IsNotNull(objResult);
			Assert.AreEqual(400, objResult.StatusCode);
		}

		[TestMethod]
		public void AddGame_Should_Return_204()
		{
			//Arrange
			MockData();
			var controller = new Controllers.UsersController(iLogger, iUsers, iAppSettings);

			//Act
			var task = controller.AddGame(1, new Inputs.AddGameInput { gameId = 33 }); //Game should be added successfully.
			task.Wait();
			var result = task.Result;
			var objResult = result as NoContentResult;

			//Assert
			Assert.IsNotNull(objResult);
			Assert.AreEqual(204, objResult.StatusCode);
		}

		[TestMethod]
		public void DeleteGame_Should_Return_404_BadUser()
		{
			//Arrange
			MockData();
			var controller = new Controllers.UsersController(iLogger, iUsers, iAppSettings);

			//Act
			var result = controller.DeleteGame(5, 31); //User doesn't exist.
			var objResult = result as NotFoundResult;

			//Assert
			Assert.IsNotNull(objResult);
			Assert.AreEqual(404, objResult.StatusCode);
		}

		[TestMethod]
		public void DeleteGame_Should_Return_404_BadGame()
		{
			//Arrange
			MockData();
			var controller = new Controllers.UsersController(iLogger, iUsers, iAppSettings);

			//Act
			var result = controller.DeleteGame(1, 99); //User doesn't have game in list.
			var objResult = result as NotFoundResult;

			//Assert
			Assert.IsNotNull(objResult);
			Assert.AreEqual(404, objResult.StatusCode);
		}

		[TestMethod]
		public void DeleteGame_Should_Return_204()
		{
			//Arrange
			MockData();
			var controller = new Controllers.UsersController(iLogger, iUsers, iAppSettings);

			//Act
			var result = controller.DeleteGame(1, 31); //Should delete game.
			var objResult = result as NoContentResult;

			//Assert
			Assert.IsNotNull(objResult);
			Assert.AreEqual(204, objResult.StatusCode);
		}

		[TestMethod]
		public void CompareGames_Should_Return_404()
		{
			//Arrange
			MockData();
			var controller = new Controllers.UsersController(iLogger, iUsers, iAppSettings);

			//Act
			var result = controller.CompareGames(5, new Inputs.ComparisonInput { otherUserId = 2, comparison = "union" }); //User doesn't exist.
			var objResult = result as NotFoundResult;

			//Assert
			Assert.IsNotNull(objResult);
			Assert.AreEqual(404, objResult.StatusCode);
		}

		[TestMethod]
		public void CompareGames_Should_Return_400_BadOtherUser()
		{
			//Arrange
			MockData();
			var controller = new Controllers.UsersController(iLogger, iUsers, iAppSettings);

			//Act
			var result = controller.CompareGames(1, new Inputs.ComparisonInput { otherUserId = 5, comparison = "union" }); //Other user doesn't exist.
			var objResult = result as BadRequestResult;

			//Assert
			Assert.IsNotNull(objResult);
			Assert.AreEqual(400, objResult.StatusCode);
		}

		[TestMethod]
		public void CompareGames_Should_Return_400_BadComparison()
		{
			//Arrange
			MockData();
			var controller = new Controllers.UsersController(iLogger, iUsers, iAppSettings);

			//Act
			var result = controller.CompareGames(1, new Inputs.ComparisonInput { otherUserId = 2, comparison = "fake_comparison" }); //Comparison doesn't exist.
			var objResult = result as BadRequestResult;

			//Assert
			Assert.IsNotNull(objResult);
			Assert.AreEqual(400, objResult.StatusCode);
		}

		[TestMethod]
		public void CompareGames_Should_Return_200_Union()
		{
			//Arrange
			MockData();
			var controller = new Controllers.UsersController(iLogger, iUsers, iAppSettings);

			//Act
			var result = controller.CompareGames(1, new Inputs.ComparisonInput { otherUserId = 2, comparison = "union" }); //Should return comparison.
			var objResult = result as OkObjectResult;
			var value = objResult.Value as Outputs.ComparisonOutput;

			//Assert
			Assert.IsNotNull(objResult);
			Assert.AreEqual(200, objResult.StatusCode);

			Assert.IsNotNull(value);
			Assert.AreEqual(1, value.userId);
			Assert.AreEqual(2, value.otherUserId);
			Assert.AreEqual("union", value.comparison);
			Assert.AreEqual(3, value.games.Count);
			Assert.AreEqual(31, value.games[0].gameId);
			Assert.AreEqual(32, value.games[1].gameId);
			Assert.AreEqual(33, value.games[2].gameId);
		}

		[TestMethod]
		public void CompareGames_Should_Return_200_Intersection()
		{
			//Arrange
			MockData();
			var controller = new Controllers.UsersController(iLogger, iUsers, iAppSettings);

			//Act
			var result = controller.CompareGames(1, new Inputs.ComparisonInput { otherUserId = 2, comparison = "intersection" }); //Should return comparison.
			var objResult = result as OkObjectResult;
			var value = objResult.Value as Outputs.ComparisonOutput;

			//Assert
			Assert.IsNotNull(objResult);
			Assert.AreEqual(200, objResult.StatusCode);

			Assert.IsNotNull(value);
			Assert.AreEqual(1, value.userId);
			Assert.AreEqual(2, value.otherUserId);
			Assert.AreEqual("intersection", value.comparison);
			Assert.AreEqual(1, value.games.Count);
			Assert.AreEqual(32, value.games[0].gameId);
		}

		[TestMethod]
		public void CompareGames_Should_Return_200_Difference()
		{
			//Arrange
			MockData();
			var controller = new Controllers.UsersController(iLogger, iUsers, iAppSettings);

			//Act
			var result = controller.CompareGames(1, new Inputs.ComparisonInput { otherUserId = 2, comparison = "difference" }); //Should return comparison.
			var objResult = result as OkObjectResult;
			var value = objResult.Value as Outputs.ComparisonOutput;

			//Assert
			Assert.IsNotNull(objResult);
			Assert.AreEqual(200, objResult.StatusCode);

			Assert.IsNotNull(value);
			Assert.AreEqual(1, value.userId);
			Assert.AreEqual(2, value.otherUserId);
			Assert.AreEqual("difference", value.comparison);
			Assert.AreEqual(1, value.games.Count);
			Assert.AreEqual(33, value.games[0].gameId);
		}

		#endregion

		#region [ Mock ]

		//Ordinarily, I would make a mock version of Data.Users for testing...
		//but, since this is only doing read-only operations from an API, and saving to an in-memory data store...
		//doing this will save a little development time.

		//It'd be worth spending that extra time in a real-world application, though.

		private ILogger<Controllers.UsersController> iLogger;
		private Data.IUsers iUsers;
		private Models.IAppSettings iAppSettings;

		private void ClearData()
		{
			iUsers = new Data.Users();
		}

		private void MockData()
		{
			ClearData();

			iUsers.list.Add(new Data.User {
				userId = 1,
				games = new List<Data.Game> {
					new Data.Game { gameId = 31 },
					new Data.Game { gameId = 32 }
				}
			});

			iUsers.list.Add(new Data.User {
				userId = 2,
				games = new List<Data.Game> {
					new Data.Game { gameId = 32 },
					new Data.Game { gameId = 33 }
				}
			});

			iUsers.list.Add(new Data.User {
				userId = 3,
				games = new List<Data.Game> {
					new Data.Game { gameId = 33 },
					new Data.Game { gameId = 34 }
				}
			});

		}

		#endregion
	}
}
