using System;

namespace VideoGameAPI.Models.RawgApi
{
	public class Game
	{
		public int? id { get; set; }
		public string name { get; set; }
		public int? added { get; set; }
		public int? metacritic { get; set; }
		public Decimal? rating { get; set; }
		public string released { get; set; }
		public string updated { get; set; }
	}
}
