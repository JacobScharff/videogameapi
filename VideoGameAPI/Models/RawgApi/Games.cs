using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using System.Web;

namespace VideoGameAPI.Models.RawgApi
{
	public class Games
	{
		public Games()
		{
			results = new List<Game>();
		}

		public async Task Search(string apiKey, string search, string ordering = null)
		{
			var temp = new RawgApi.Games();

			string url = $@"https://api.rawg.io/api/games?key={apiKey}&search={HttpUtility.UrlEncode(search)}";
			if (!string.IsNullOrEmpty(ordering)) url += $@"&ordering={HttpUtility.UrlEncode(ordering)}";

			using (HttpClient client = new HttpClient())
			{
				while (url != null)
				{
					using (var response = await client.GetAsync(url))
					{
						if (response.IsSuccessStatusCode)
						{
							if (response.Content is object && response.Content.Headers.ContentType.MediaType == "application/json")
							{
								var jsonString = await response.Content.ReadAsStringAsync();
								var jsonList = JsonSerializer.Deserialize<RawgApi.Games>(jsonString);
								temp.results.AddRange(jsonList.results);

								url = jsonList.next;
							}
							else
							{
								throw new Exception("RAWG.IO returned unexpected content for URL: " + url);
							}
						}
						else
						{
							throw new Exception("RAWG.IO returned status code " + response.StatusCode + " for URL: " + url);
						}
					}
				}
			}

			results = temp.results;
		}

		public async Task<Game> Get(string apiKey, int id)
		{
			Game game = null;

			using (HttpClient client = new HttpClient())
			{
				string url = $@"https://api.rawg.io/api/games/{HttpUtility.UrlEncode(id.ToString())}?key={apiKey}";

				using (var response = await client.GetAsync(url))
				{
					if (response.IsSuccessStatusCode)
					{
						if (response.Content is object && response.Content.Headers.ContentType.MediaType == "application/json")
						{
							var jsonString = await response.Content.ReadAsStringAsync();
							game = JsonSerializer.Deserialize<RawgApi.Game>(jsonString);
						}
						else
						{
							throw new Exception("RAWG.IO returned unexpected content for URL: " + url);
						}
					}
					else
					{
						throw new Exception("RAWG.IO returned status code " + response.StatusCode + " for URL: " + url);
					}
				}
			}

			return game;
		}

		public string next { get; set; }
		public List<Game> results { get; set; }
	}
}
