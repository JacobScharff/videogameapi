using System;
using System.ComponentModel.DataAnnotations;

namespace VideoGameAPI.Models.Inputs
{
	public class ComparisonInput
	{
		public int otherUserId { get; set; }
		public string comparison { get; set; }
	}
}
