using System;

namespace VideoGameAPI.Models.Inputs
{
	public class AddGameInput
	{
		public int gameId { get; set; }
	}
}
