using System;
using System.Collections.Generic;

namespace VideoGameAPI.Models.Outputs
{
	public class ComparisonOutput
	{
		public ComparisonOutput()
		{
			games = new List<Data.Game>();
		}

		public int userId { get; set; }
		public int otherUserId { get; set; }
		public string comparison { get; set; }
		public List<Data.Game> games { get; set; }
	}
}
