using System;
using System.Collections.Generic;

namespace VideoGameAPI.Models.Data
{
	public class Games
	{
		public Games()
		{
			list = new List<Game>();
		}

		public List<Game> list { get; set; }
	}
}
