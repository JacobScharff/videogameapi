using System;
using System.Collections.Generic;
using System.Linq;

namespace VideoGameAPI.Models.Data
{
	public interface IUsers
	{
		#region [ Methods ]

		public User Add();
		public User Get(int userId);

		#endregion

		#region [ Properties ]

		private static int NextId;
		public List<User> list { get; set; }

		#endregion
	}

	public class Users : IUsers
	{
		#region [ Constructors ]

		public Users()
		{
			list = new List<User>();
		}

		#endregion

		#region [ Methods ]

		public User Add()
		{
			NextId++;

			var newUser = new User { userId = NextId };
			list.Add(newUser);

			return newUser;
		}

		public User Get(int userId)
		{
			return list.Where(x => x.userId == userId).FirstOrDefault();
		}

		#endregion

		#region [ Properties ]

		private static int NextId = 0;
		public List<User> list { get; set; }

		#endregion
	}
}
