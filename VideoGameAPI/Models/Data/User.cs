using System;
using System.Collections.Generic;
using System.Linq;

namespace VideoGameAPI.Models.Data
{
	public interface IUser
	{
		#region [ Methods ]

		public void AddGame(Game game);

		public bool HasGame(int gameId);

		public void DeleteGame(int gameId);

		#endregion

		#region [ Properties ]

		public int userId { get; set; }
		public List<Game> games { get; set; }

		#endregion
	}

	public class User : IUser
	{
		#region [ Constructors ]

		public User()
		{
			games = new List<Game>();
		}

		#endregion

		#region [ Methods ]

		public void AddGame(Game game)
		{
			games.Add(game);
		}

		public bool HasGame(int gameId)
		{
			return games.Exists(x => x.gameId == gameId);
		}

		public void DeleteGame(int gameId)
		{
			var game = games.Where(x => x.gameId == gameId).FirstOrDefault();

			if (game != null)
			{
				games.Remove(game);
			}
		}

		#endregion

		#region [ Properties ]

		public int userId { get; set; }
		public List<Game> games { get; set; }

		#endregion
	}
}
