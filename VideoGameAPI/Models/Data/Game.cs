using System;

namespace VideoGameAPI.Models.Data
{
	public class Game
	{
		public int? gameId { get; set; }
		public string name { get; set; }
		public int? added { get; set; }
		public int? metacritic { get; set; }
		public Decimal? rating { get; set; }
		public string released { get; set; }
		public string updated { get; set; }

		public override bool Equals(object obj)
		{
			if (obj == null || !(obj is Game)) return false;
			var game = (Game)obj;
			return this.gameId == game.gameId;
		}

		public override int GetHashCode()
		{
			return this.gameId.GetHashCode();
		}
	}
}
