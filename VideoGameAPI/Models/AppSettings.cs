using System;

namespace VideoGameAPI.Models
{
	public interface IAppSettings
	{
		public string RawgApiKey { get; set; }
	}

	public class AppSettings : IAppSettings
	{
		public string RawgApiKey { get; set; }
	}
}
