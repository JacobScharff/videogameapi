﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;
using System;
using VideoGameAPI.Models;
using Data = VideoGameAPI.Models.Data;
using Inputs = VideoGameAPI.Models.Inputs;
using Outputs = VideoGameAPI.Models.Outputs;
using RawgApi = VideoGameAPI.Models.RawgApi;

namespace VideoGameAPI.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class UsersController : ControllerBase
	{
		private readonly ILogger<UsersController> _logger;
		private readonly Data.IUsers _users;
		private readonly IAppSettings _appSettings;

		public UsersController(ILogger<UsersController> logger, Data.IUsers users, IAppSettings appSettings)
		{
			_logger = logger;
			_users = users;
			_appSettings = appSettings;
		}

		[HttpPost]
		public IActionResult AddUser()
		{
			try
			{
				var newUser = _users.Add();
				var newUri = $@"/users/{newUser.userId}";

				return Created(newUri, newUser);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, ex.Message, null);
				return StatusCode(500);
			}
		}

		[HttpGet]
		[Route("{userId:int}")]
		public IActionResult GetUser(int userId)
		{
			try
			{
				var user = _users.Get(userId);

				if (user == null) return NotFound();

				return Ok(user);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, ex.Message, null);
				return StatusCode(500);
			}
		}

		[HttpPost]
		[Route("{userId:int}/games")]
		public async Task<IActionResult> AddGame(int userId, Inputs.AddGameInput input)
		{
			try
			{
				var user = _users.Get(userId);

				if (user == null) return NotFound();
				if (user.HasGame(input.gameId)) return Conflict();
				if (input.gameId <= 0) return BadRequest();

				var games = new RawgApi.Games();
				var game = await games.Get(_appSettings.RawgApiKey, input.gameId);

				if (game == null) return BadRequest();

				user.AddGame(new Data.Game {
					gameId = game.id,
					name = game.name,
					added = game.added,
					metacritic = game.metacritic,
					rating = game.rating,
					released = game.released,
					updated = game.updated
				});
				return NoContent();
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, ex.Message, null);
				return StatusCode(500);
			}
		}

		[HttpDelete]
		[Route("{userId:int}/games/{gameId:int}")]
		public IActionResult DeleteGame(int userId, int gameId)
		{
			try
			{
				var user = _users.Get(userId);

				if (user == null) return NotFound();
				if (!user.HasGame(gameId)) return NotFound();

				user.DeleteGame(gameId);
				return NoContent();
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, ex.Message, null);
				return StatusCode(500);
			}
		}

		private static readonly string[] ComparisonValues = new[]
		{
			"union", "intersection", "difference"
		};

		[HttpPost]
		[Route("{userId:int}/comparison")]
		public IActionResult CompareGames(int userId, Inputs.ComparisonInput input)
		{
			try
			{
				var user1 = _users.Get(userId);
				var user2 = _users.Get(input.otherUserId);

				if (user1 == null) return NotFound();
				if (user2 == null) return BadRequest();
				if (!ComparisonValues.Contains(input.comparison.ToLower())) return BadRequest();

				var output = new Outputs.ComparisonOutput()
				{
					userId = userId,
					otherUserId = input.otherUserId,
					comparison = input.comparison.ToLower()
				};

				switch (input.comparison.ToLower())
				{
					case "union":
						output.games = user1.games.Union(user2.games).ToList();
						break;

					case "intersection":
						output.games = user1.games.Intersect(user2.games).ToList();
						break;

					case "difference":
						output.games = user2.games.Except(user1.games).ToList();
						break;

					default:
						break;
				}
				return Ok(output);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, ex.Message, null);
				return StatusCode(500);
			}
		}

	}
}
