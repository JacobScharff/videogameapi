﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;
using System;
using VideoGameAPI.Models;
using Data = VideoGameAPI.Models.Data;
using RawgApi = VideoGameAPI.Models.RawgApi;

namespace VideoGameAPI.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class GamesController : ControllerBase
	{
		private readonly ILogger<GamesController> _logger;
		private readonly IAppSettings _appSettings;

		public GamesController(ILogger<GamesController> logger, IAppSettings appSettings)
		{
			_logger = logger;
			_appSettings = appSettings;
		}

		private static readonly string[] SortValues = new[]
		{
			"name", "released", "added", "created", "updated", "rating", "metacritic"
		};

		[HttpGet]
		public async Task<IActionResult> Search(string q = null, string sort = null)
		{
			try
			{
				if (string.IsNullOrEmpty(q)) return BadRequest();
				if (sort != null && !SortValues.Contains(sort.Replace("-", ""))) return BadRequest();

				var dataGames = new Data.Games();

				var rawgGames = new RawgApi.Games();
				await rawgGames.Search(_appSettings.RawgApiKey, q, sort);

				dataGames.list = rawgGames.results.Select(x => new Data.Game()
				{
					gameId = x.id,
					name = x.name,
					added = x.added,
					metacritic = x.metacritic,
					rating = x.rating,
					released = x.released,
					updated = x.updated
				}).ToList();

				return Ok(dataGames.list);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, ex.Message, null);
				return StatusCode(500);
			}
		}

	}
}
